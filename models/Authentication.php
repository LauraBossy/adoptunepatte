<?php
require_once('connect.php');

function Authentication($name, $password) {

    global $PDO;
    
    $req = "SELECT * FROM `admin` WHERE `name`= :name";

    $preparedReq = $PDO->prepare($req);
    
    $preparedReq->execute(
        array(
            "name" => $name
        )
    );
    // PDO::FETCH_ASSOC est une méthode pour afficher les informations seulement avec les titres des colonnes (pour éviter doublons inutiles).
    $result = $preparedReq->fetch(PDO::FETCH_ASSOC);
    
    if (!$result || !password_verify($password, $result['password'])) {
        echo "<script> alert('L\'utilisateur et/ou le mot de passe n\'existe pas'); </script>";
    }

    else {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
          }

          $_SESSION['user'] = [
            "name" => $result['name']
          ];

          // Attention : pas d'espace entre Location et les ":"
          header('Location: admin');
    }

    require_once('close.php');
}

function SessionStart() {
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
}

function UserRedirection() {
    if (!isset($_SESSION['user'])) {
        header('Location: login');
    }
}

function AdminRedirection() {
    if (isset($_SESSION['user'])) {
        header('Location: admin');
      }
}

function Login($name, $password) {
    if (!empty($_POST)) {
        if ( isset($_POST[$name]) && !empty($_POST[$name]) 
             && isset($_POST[$password]) && !empty($_POST[$password]) ) {
      
              Authentication($_POST[$name], $_POST[$password]);
              }
      }
}