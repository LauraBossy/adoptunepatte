<?php 
require_once('connect.php');

function GetPaws() {
    global $PDO;

    $req = "SELECT * FROM animals;";
    $preparedReq = $PDO->prepare($req);
    $preparedReq->execute();
    $result = $preparedReq->fetchAll(PDO::FETCH_ASSOC);
    return $result; 
    require_once('close.php');
}

function GetOnePaw($id) {
    global $PDO;

    // Clean up infos to protect from hack attacks
    $cleanId = strip_tags($id);

    $req = "SELECT * FROM animals WHERE `id` = :id;";
    $preparedReq = $PDO->prepare($req);
    $preparedReq->execute(
        array('id' => $cleanId)
    );
    $result = $preparedReq->fetch();
    return $result; 
    require_once('close.php');
}

function CreatePaw($name, $age, $gender, $img_name) {
    global $PDO;

    $cleanName = strip_tags($name);
    $cleanAge = strip_tags($age);
    $cleanGender = strip_tags($gender);
    $cleanImgName = strip_tags($img_name);
    
    $req = "INSERT INTO animals (`name`, `age`, `gender`, `img_name`) VALUES (:name, :age, :gender, :img_name);";
    $preparedReq = $PDO->prepare($req);
    $preparedReq->execute(
                    array(
                        "name" => $cleanName,
                        "age" => $cleanAge,
                        "gender" => $cleanGender,
                        "img_name" => $cleanImgName
                    )
                  );
    require_once('close.php');
}

function UpdateMyPaw($id, $name, $age, $gender, $img_name) {
    global $PDO;

    // Clean up infos to protect from hack attacks
    $cleanId = strip_tags($id);
    $cleanName = strip_tags($name);
    $cleanAge = strip_tags($age);
    $cleanGender = strip_tags($gender);
    $cleanImgName = strip_tags($img_name);

    $req = "UPDATE `animals` SET `name` =  :name , `age` = :age, `gender` = :gender, `img_name` = :img_name WHERE `id` = :id;";
    $preparedReq = $PDO->prepare($req);
    $preparedReq->execute(
        array(
            "id" => $cleanId,
            "name" => $cleanName,
            "age" => $cleanAge,
            "gender" => $cleanGender,
            "img_name" => $cleanImgName
        )
      );
    require_once('close.php');
}
    
function DeletePaw($id) {
    global $PDO;

    // Clean up infos to protect from hack attacks
    $cleanId = strip_tags($id);

    $req = " DELETE FROM `animals` WHERE `id` = :id;";
    $preparedReq = $PDO->prepare($req);
    $preparedReq->execute(
        array('id' => $cleanId)
      );
    require_once('close.php');
}

?>