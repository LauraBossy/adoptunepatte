<?php
if ($_POST) {
    if ( !empty($_FILES['image'])
         && isset($_POST['id']) && !empty($_POST['id']) 
         && isset($_POST['updateName']) && !empty($_POST['updateName'])
         && isset($_POST['updateAge']) && !empty($_POST['updateAge'])
         && isset($_POST['updateGender']) && !empty($_POST['updateGender']) ) {
            $img_name = $_FILES['image']['name'];

            $tmp_name = $_FILES['image']['tmp_name'];

            // set a unique image name
            $time = time();

            $new_img_name = $time.$img_name;

            $move_img = move_uploaded_file($tmp_name, "./library/assets/image_bdd/".$new_img_name);

            if ($move_img) {
                UpdateMyPaw($_POST['id'], $_POST['updateName'], $_POST['updateAge'], $_POST['updateGender'], $new_img_name);
                $_SESSION['message'] = 'Les modifications ont bien été prises en compte';
                header('Location: admin');
            }
            else {
                $_SESSION['erreur'] = 'Veuillez choisir une image inférieure à 1Mo';
            }
    }
    else {
        $_SESSION['erreur'] = 'Le formulaire est incomplet';
    }
}

if (isset($_GET['id']) && !empty($_GET['id'])) {
    $onePaw = GetOnePaw($_GET['id']);

    if(!$onePaw){
        $_SESSION['erreur'] = 'Cet id n\'existe pas';
        header('Location: admin');
    }
} 
else {
    $_SESSION['erreur'] = 'URL invalide';
    header('Location: admin');
}
?>