<?php
if ($_POST) {
    if ( !empty($_FILES['image'])
         && isset($_POST['name']) && !empty($_POST['name'])
         && isset($_POST['age']) && !empty($_POST['age'])
         && isset($_POST['gender']) && !empty($_POST['gender']) ) {
            $img_name = $_FILES['image']['name'];

            $tmp_name = $_FILES['image']['tmp_name'];

            // set a unique image name
            $time = time();

            $new_img_name = $time.$img_name;

            $move_img = move_uploaded_file($tmp_name, "./library/assets/image_bdd/".$new_img_name);

            if ($move_img) {
                CreatePaw($_POST['name'], $_POST['age'], $_POST['gender'], $new_img_name);
                $_SESSION['message'] = 'Bienvenue à votre patte !';
                header('Location: admin');
            }
            else {
                $_SESSION['erreur'] = 'Veuillez choisir une image inférieure à 1Mo';
            }
    }
    else {
        $_SESSION['erreur'] = 'Le formulaire est incomplet';
        
        if (!empty($_SESSION['erreur'])) {
            echo "<script type='text/javascript'> alert(' " . $_SESSION['erreur'] . " '); </script>";  
            $_SESSION['erreur'] = ''; 
        }
    }
}

?>