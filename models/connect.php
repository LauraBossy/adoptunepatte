<?php

$user = "root";
$pwd = "";
$host = "localhost";
$port = "3306";
$dbname = "adoptunepatte";
$dsn = "mysql:host=$host;port=$port;dbname=$dbname";

try {
    $PDO = new PDO($dsn, $user, $pwd);
    $PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch (PDOException $e) {
    echo "Error : " . $e->getMessage();
    die();
}