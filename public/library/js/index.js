// TODO : fetch the array instead of set it up here
function GetDogsBreeds() {
    const result = [
        "affenpinscher",
        "african",
        "airedale",
        "akita",
        "appenzeller",
        "australian",
        "basenji",
        "beagle",
        "bluetick",
        "borzoi",
        "bouvier",
        "boxer",
        "brabancon",
        "briard",
        "buhund",
        "bulldog",
        "bullterrier",
        "cattledog",
        "chihuahua",
        "chow",
        "clumber",
        "cockapoo",
        "collie",
        "coonhound",
        "corgi",
        "cotondetulear",
        "dachshund",
        "dalmatian",
        "dane",
        "deerhound",
        "dhole",
        "dingo",
        "doberman",
        "elkhound",
        "entlebucher",
        "eskimo",
        "finnish",
        "frise",
        "germanshepherd",
        "greyhound",
        "groenendael",
        "havanese",
        "hound",
        "husky",
        "keeshond",
        "kelpie",
        "komondor",
        "kuvasz",
        "labradoodle",
        "labrador",
        "leonberg",
        "lhasa",
        "malamute",
        "malinois",
        "maltese",
        "mastiff",
        "mexicanhairless",
        "mix",
        "mountain",
        "newfoundland",
        "otterhound",
        "ovcharka",
        "papillon",
        "pekinese",
        "pembroke",
        "pinscher",
        "pitbull",
        "pointer",
        "pomeranian",
        "poodle",
        "pug",
        "puggle",
        "pyrenees",
        "redbone",
        "retriever",
        "ridgeback",
        "rottweiler",
        "saluki",
        "samoyed",
        "schipperke",
        "schnauzer",
        "setter",
        "sharpei",
        "sheepdog",
        "shiba",
        "shihtzu",
        "spaniel",
        "springer",
        "stbernard",
        "terrier",
        "tervuren",
        "vizsla",
        "waterdog",
        "weimaraner",
        "whippet",
        "wolfhound"
    ];

    // fetch('https://dog.ceo/api/breeds/list/all')
    //     .then(response => response.json())
    //     .then(all => {
    //         for (var i in all.message)
    //             result.push(i);
    //     });
    // console.log(result);
    // console.log(result.length);
    return result;
}

// TODO : why it's displaying only one div ?
function GetDogPicture() {

    const gridimg = document.getElementById("gridimg");
    const div = document.createElement('div');
    var img = document.createElement('img');

    for (let i = 0; i < GetDogsPicturesURL().length; i++) {
        fetch(GetDogsPicturesURL()[i])
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                div.className = "grid-OneItem";
                img.src = `${data.message}`;

                div.appendChild(img);
            })
            .catch(function (error) {
                console.log(error);
            });

        gridimg.appendChild(div);
    }
}

function GetDogsPicturesURL() {
    var url = '';
    var urlArray = [];

    for (let i = 0; i < GetDogsBreeds().length; i++) {
        url = 'https://dog.ceo/api/breed/' + GetDogsBreeds()[i] + '/images/random';
        urlArray.push(url);
    }
    return urlArray;
}

const CreateModalContainer = document.querySelector(".createModal-container");
const CreateModalTriggers = document.querySelectorAll(".addPaws");

CreateModalTriggers.forEach(trigger => trigger.addEventListener("click", PopupCreate))

function PopupCreate() {
    CreateModalContainer.classList.toggle("active");
}


// const UpdateModalContainer = document.querySelector(".updateModal-container");
// const UpdateModalTriggers = document.querySelectorAll(".updatePaws");

// UpdateModalTriggers.forEach(trigger => trigger.addEventListener("click", PopupUpdate))

// function PopupUpdate() {
//     UpdateModalContainer.classList.toggle("active");
// }

