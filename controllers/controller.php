<?php
require_once('../models/AnimalsManagement.php');
require_once('../models/Authentication.php');

$action = substr(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), 1);

SessionStart();

switch ($action) {

    case 'login':
        AdminRedirection();
        Login('nom', 'motdepasse');
        include "../views/LoginPage.php";
        break;

    case 'admin':
        UserRedirection();
        include "../views/AdminPage.php";
        break;

    case 'logout':
        UserRedirection();
        include "../models/Logout.php";
        break;

    case 'update':
        UserRedirection();
        require_once('../models/Update.php');
        include "../views/UpdatePage.php";
        break;

    case 'delete':
        UserRedirection();
        include "../models/Delete.php";
        break;

    case 'create':
        UserRedirection();
        require_once('../models/Create.php');
        include "../views/CreatePage.php";
        break;

    default:  
        include "../views/HomePage.php";
        break;
}
