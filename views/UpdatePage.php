<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="./library/css/adminPage.css">
    <link rel="stylesheet" href="./library/css/create-update.css" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&family=Raleway:wght@100;200;300;400;500;600;700;800;900&display=swap"
      rel="stylesheet"
    />
    <title> Modifier les infos d'une patte </title>
</head>

<body>
    <?php 
        include('../views/Headers/header.php');

        if(!empty($_SESSION['erreur'])) {
            echo "<script type='text/javascript'> alert('" . $_SESSION['erreur'] . " '); </script>";  
            $_SESSION['erreur'] = '';
        }
    ?>

    <div class="updateModal-container">
        <div class="modal" role="dialog" aria-labelledby="modalTitle">
            <a href="admin">
                <button 
                    aria-label="close modal"
                    class="close-modal addPaws">
                        X
                </button>
            </a>
            <div id="createPawInfos">
                <h1 id="modalTitle"> Changer les informations de votre patte </h1>
                <form method="post" class="AllPawInfos" enctype="multipart/form-data">
                    <div class="PawInfos">
                        <label for="image"> Sa bouille : </label>
                        <input type="file" id="image" name="image" />
                    </div>
                    <div class="PawInfos">
                        <label for="updateName"> Son nom : </label>
                        <input type="text" id="updateName" name="updateName" value=" <?php echo $onePaw["name"] ; ?>"/>
                    </div>
                    <div class="PawInfos">
                        <label for="updateAge"> Son âge : </label>
                        <input type="text" id="updateAge" name="updateAge" value=" <?php echo $onePaw["age"] ; ?> "/>
                    </div>
                    <div class="PawInfos">
                        <label for="updateGender"> Son genre : </label>
                        <input type="text" id="updateGender" name="updateGender" value=" <?php echo $onePaw['gender'] ; ?> "/>
                    </div>
                    <div class="PawInfos">
                        <input type="hidden" value="<?php echo $onePaw['id'] ; ?>" name="id"/>
                    </div>
                    <input type="submit" name="submit" class="#" value="Enregistrer les modifications" />
                </form>
            </div>
        </div>
    </div>
</body>
</html>