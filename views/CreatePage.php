<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="./library/css/adminPage.css">
    <link rel="stylesheet" href="./library/css/create-update.css" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&family=Raleway:wght@100;200;300;400;500;600;700;800;900&display=swap"
      rel="stylesheet"
    />
    <title> Bienvenue à votre patte </title>
</head>

<body>
    <?php 
        include('../views/Headers/header.php');
    ?>

    <div class="updateModal-container">
        <div class="modal" role="dialog" aria-labelledby="modalTitle">
            <a href="admin">
                <button 
                    aria-label="close modal"
                    class="close-modal addPaws">
                        X
                </button>
            </a>
            <div id="createPawInfos">
                <h1 id="modalTitle"> Présentez votre patte </h1>
                <form method="post" class="AllPawInfos" enctype="multipart/form-data">
                    <div class="PawInfos">
                        <label for="image"> Sa bouille : </label>
                        <input type="file" id="image" name="image" />
                    </div>
                    <div class="PawInfos">
                        <label for="name"> Son nom : </label>
                        <input type="text" id="name" name="name" />
                    </div>
                    <div class="PawInfos">
                        <label for="gender"> Son genre : </label>
                        <input type="text" id="gender" name="gender" placeholder="male/femelle"/>
                    </div>
                    <div class="PawInfos">
                        <label for="age"> Son âge : </label>
                        <input type="text" id="age" name="age" placeholder="6"/>
                    </div>
                    <input type="submit" name="submit" value="Inscrire ma patte" />
                </form>
            </div>
        </div>
    </div>
</body>
</html>