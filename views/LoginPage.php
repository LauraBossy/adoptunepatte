<!DOCTYPE html>
<html lang="en" >
  <head>
    <meta charset="UTF-8">
    <title> Se connecter </title>
    <link rel="stylesheet" href="./library/css/loginPage.css">
  </head>

  <body>

    <?php 
      include('../views/Headers/loginHeader.php');
    ?>
   <div class="main">
    <div class="login-page">
      <div class="form">
        <form class="login-form" action="" method="post">
          <input type="text" name="nom" id="nom" placeholder="Nom"/>
          <input type="password" name="motdepasse" id="motdepasse" placeholder="Mot de passe"/>
          <input type="submit" value="Se connecter" name="submit" id="submit" />
        </form>
      </div>
    </div>
  </div>
  
    <?php 
      include('../views/Footers/homeFooter.php');
    ?>

  </body>
</html>