<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./library/css/adminPage.css">
    <title>Adopt'unepatte</title>
</head>

<body>
    <?php 
        include('../views/Headers/header.php');
    ?>

<p> Bienvenue ! </br> Ici vous pouvez rencontrer la future patte de votre vie ! Contactez-nous au <a href="tel:+336 12 34 56 78 " id="call"> 06 12 34 56 78</a>  pour plus d'infos sur une patte ou si vous avez le coup de coeur </p>


<div class="main">

        <div id="titleAndGallery">

            <div id="gridimg" class="grille">

            <?php
                foreach (GetPaws() as $paw ) {
            ?>
                <div class="grid-item"> 
                    <img src="./library/assets/image_bdd/<?php echo $paw["img_name"] ?>" class="image">
                    <div class="middle">
                        <div class="text">
                            <p> <?php echo $paw["name"]; ?> </p>
                            <p> <?php echo $paw["age"]; ?> an(s) </p>
                            <p> <?php echo $paw["gender"]; ?> </p>
                        </div>
                    </div>
                </div>
            <?php
                }
            ?>
            </div>
        </div>
    </div>

    <?php 
        include('../views/Footers/homeFooter.php');
    ?>
    
    <script src="./library/js/index.js"></script>
</body>
</html>