<header>
    <nav>
        <a href="HomePage.php" class="nav-icon" aria-label="visit homepage" aria-current="page"> 
            <img src="library/assets/adoptlogo.png" alt="logo">
            <span> Adopt'une patte !</span>
        </a>

        <div id="menu">
            <div class="main-navlinks">
                <button class="hamburger" type="button" aria-label="Toggle Navigation" aria-expanded="false">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <?php 
                    if (!isset($_SESSION['user'])): ?>
                        <div class="navlinks-container">
                            <a href="login"> Se connecter </a>
                        </div>
            </div>
                        <div class="nav-authentication">
                            <a href="login" class="user_toggler" aria-label="Login">
                                <img src="library/assets/user.svg" alt="user-icon">
                            </a>
                        </div> 
                <?php else: 
                      if($_SERVER["REQUEST_URI"] === '/admin') {
                ?>
                        <div class="navlinks-container">
                            <a href="logout"> Se déconnecter </a>
                        </div>
                <?php
                      }
                      else {
                ?>
                        <div class="navlinks-container">
                            <a href="admin"> Gérer mes pattes </a>
                        </div>
                        <div class="navlinks-container">
                            <a href="logout"> Se déconnecter </a>
                        </div>
                <?php
                      }
                ?>    
            </div>
                        <div class="nav-authentication">
                            <a href="logout" class="user_toggler" aria-label="Logout">
                                <img src="library/assets/logout.png" alt="user-icon" id="logout">
                            </a>
                        </div> 
                <?php endif; ?>
            </div>
        </div>  
    </nav>
</header>