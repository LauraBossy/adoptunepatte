<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="./library/css/adminPage.css">
    <link rel="stylesheet" href="./library/css/components/popupCreatePaw.css" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&family=Raleway:wght@100;200;300;400;500;600;700;800;900&display=swap"
      rel="stylesheet"
    />
    <title> Vos pattes </title>
</head>

<body>
    <?php 
        include('../views/Headers/header.php');
    ?>

    <?php
        // Id error
        if (!empty($_SESSION['erreur'])) {
            echo "<script type='text/javascript'> alert(' " . $_SESSION['erreur'] . " '); </script>";  
            $_SESSION['erreur'] = ''; 
        }
    ?>

    <?php
        // Create, Update or Delete success
        if (!empty($_SESSION['message'])) {
            echo "<script type='text/javascript'> alert(' " . $_SESSION['message'] . " '); </script>";  
            $_SESSION['message'] = ''; 
        }
    ?>

    <div class="main">
        <div id="titleAndGallery">

            <div id="title">
                <h1 id="adminPaws"> Gérer mes pattes </h1>
                <a href="create"> 
                    <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-plus-square" viewBox="0 0 16 16">
                        <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                        <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                    </svg>
                </a>
            </div>  

            <div id="gridimg" class="grille">

            <?php
                foreach (GetPaws() as $paw ) {
            ?>
                <div class="grid-item"> 
                    <img src="./library/assets/image_bdd/<?php echo $paw["img_name"] ?>" class="image">
                    <div class="middle">
                        <div class="text">
                            <p> <?php echo $paw["name"]; ?> </p>
                            <p> <?php echo $paw["age"]; ?> an(s) </p>
                            <p> <?php echo $paw["gender"]; ?> </p>
                        </div>
                        <div class="modifyAndUpdate">
                            <a href="delete?id=<?php echo $paw["id"]?>"> 
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="red" class="bi bi-x-circle" viewBox="0 0 16 16">
                                    <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                    <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                                </svg>
                            </a>
                            <a href="update?id=<?php echo $paw["id"]?>"> 
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="orange" class="bi bi-pencil-square" viewBox="0 0 16 16">
                                    <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                    <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            <?php
                }
            ?>
            </div>
        </div>
    </div>

    <script src="./library/js/index.js"> </script>
</body>
</html>